cara menjalankan apps:

1. requirements:
   - java v17.0.7
   - maven v3.9.2
2. konfigurasi database
   - buat database baru di PostgreSQL
   - buka direktori src > main > resouce > application.properties
   - sesuaikan nama db, username dan password
3. install dependency
   - run: mvn install
4. memulai Apps
   - run: mvn spring-boot
