package com.example.simulasiTest.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.simulasiTest.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("SELECT e FROM Employee e WHERE e.name = :name_employee")
	Optional<Employee> findEmployeeByName(@Param("name_employee") String name);

    @Query("SELECT e FROM Employee e WHERE e.nik = :nik_employee")
	Optional<Employee> findEmployeeByNik(@Param("nik_employee") Integer nik);
}
