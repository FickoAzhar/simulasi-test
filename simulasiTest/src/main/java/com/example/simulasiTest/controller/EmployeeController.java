package com.example.simulasiTest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.simulasiTest.Repository.EmployeeRepository;
import com.example.simulasiTest.model.Employee;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
    
    @Autowired
    EmployeeRepository employeRepo;

    @GetMapping("/employee")
    public ResponseEntity<Object> getAllEmployee() {
        try {
        Map<String, Object> result = new HashMap<String, Object>();
        List<Employee> listAllemployee = employeRepo.findAll();
        HttpStatus status = HttpStatus.OK;
        List<Employee> listAllEmployeNew = new ArrayList<>();
        for (Employee employeEntity : listAllemployee) {
            Employee emp = new Employee();
            emp.setId(employeEntity.getId());
            emp.setName(employeEntity.getName());
            emp.setNik(employeEntity.getNik());
            emp.setAddress(employeEntity.getAddress());
            emp.setBirthDate(employeEntity.getBirthDate());
            emp.setAge(employeEntity.getAge());
            emp.setMaritalStatus(employeEntity.getMaritalStatus());
            emp.setInitialContract(employeEntity.getInitialContract());
            emp.setFinalContract(employeEntity.getFinalContract());
            emp.setGender(employeEntity.getGender());
            emp.setNumOfChildren(employeEntity.getNumOfChildren());
            emp.setSalary(employeEntity.getSalary());

            listAllEmployeNew.add(emp);
        }
        if (listAllEmployeNew.isEmpty()) {
            status = HttpStatus.NOT_FOUND;
            result.put("status", "404");
            result.put("message", "Get data success but no data.");
            result.put("data", listAllEmployeNew);
            return new ResponseEntity<Object>(result, status);
        }
        result.put("data", listAllEmployeNew);
        result.put("total", listAllEmployeNew.size());
        result.put("status", "200");
        result.put("message", "Read all data employe success.");
        return new ResponseEntity<Object>(result, status);
        } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/employee/nik/{nik}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable(value = "nik") Integer nik){
        try {
            Map<String, Object> result = new HashMap<>();
            Optional<Employee> employeeData = employeRepo.findEmployeeByNik(nik);
            HttpStatus status;

            if (employeeData.isPresent()) {
                status = HttpStatus.OK;
                result.put("status", "200");
                result.put("message", "Read Data Employee Success!");
                result.put("data", employeeData.get());
            } else {
                status = HttpStatus.NOT_FOUND;
                result.put("status", "404");
                result.put("message", "Read Data Employee Failed. Id Not Found!");
            }

            return new ResponseEntity<Object>(result, status);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/employee/name/{name}")
    public ResponseEntity<Object> getEmployeeByname(@PathVariable(value = "name") String name){
        try {
            Map<String, Object> result = new HashMap<>();
            Optional<Employee> employeeData = employeRepo.findEmployeeByName(name);
            HttpStatus status;

            if (employeeData.isPresent()) {
                status = HttpStatus.OK;
                result.put("status", "200");
                result.put("message", "Read Data Employee Success!");
                result.put("data", employeeData.get());
            } else {
                status = HttpStatus.NOT_FOUND;
                result.put("status", "404");
                result.put("message", "Read Data Employee Failed. Id Not Found!");
            }

            return new ResponseEntity<Object>(result, status);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping("/create")
    public ResponseEntity<Object> createemployee(@RequestBody Employee body) {
        try {
        Map<String, Object> result = new HashMap<>();
        Employee employee = employeRepo.save(body);
        HttpStatus status = HttpStatus.CREATED;
        result.put("status", "201");
        result.put("message", "employee created successfully!");
        result.put("data", employee);
        return new ResponseEntity<Object>(result, status);
        } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateemployee(@PathVariable("id") long id, @RequestBody Employee body) {
        try {
        Map<String, Object> result = new HashMap<>();
        Optional<Employee> employeData = employeRepo.findById(id);
        HttpStatus status;
        if (employeData.isPresent()) {
            Employee employe = employeData.get();
            employe.setName(body.getName());
            employe.setNik(body.getNik());
            employe.setAddress(body.getAddress());
            employe.setBirthDate(body.getBirthDate());
            employe.setAge(body.getAge());
            employe.setMaritalStatus(body.getMaritalStatus());
            employe.setInitialContract(body.getInitialContract());
            employe.setFinalContract(body.getFinalContract());
            employe.setGender(body.getGender());
            employe.setNumOfChildren(body.getNumOfChildren());
            employe.setSalary(body.getSalary());

            employeRepo.save(employe);
            result.put("status", "200");
            result.put("message", "Data updated successfully.");
            result.put("data", employeData);
            status = HttpStatus.OK;
        } else {
            result.put("status", "404");
            result.put("message", "Update data fail. Data not found.");
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<Object>(result, status);
        } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteemployee(@PathVariable("id") long id) {
        try {
        Map<String, Object> result = new HashMap<>();
        Optional<Employee> employ = employeRepo.findById(id);
        HttpStatus status;
        if (employ.isPresent()) {
            employeRepo.deleteById(id);
            status = HttpStatus.OK;
            result.put("status", "200");
            result.put("message", "Data deleted successfully.");
        } else {
            status = HttpStatus.NOT_FOUND;
            result.put("status", "404");
            result.put("message", "Delete data fail. Data not found.");
        }
        return new ResponseEntity<Object>(result, status);
        } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}