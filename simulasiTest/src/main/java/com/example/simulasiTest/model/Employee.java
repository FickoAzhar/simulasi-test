package com.example.simulasiTest.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

@Entity
@Table(name = "user", schema= "public")
public class Employee {
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private long id;

    @Column(name = "name", nullable = false)
	private String name;

	@Column(name = "nik", nullable = false)
	private Integer nik;

	@Column(name = "address", nullable = false)
	private String address;

    @Column(name = "date_of_birth", nullable = false)
	private Date birthDate;

    @Column(name = "age", nullable = false)
	private int age;

	@Column(name = "marital_status", nullable = false)
	private Boolean maritalStatus;

    @Column(name = "initial_contract", nullable = false)
	private Date initialContract;

	@Column(name = "final_contract", nullable = false)
	private Date finalContract;

    @Column(name = "gender", nullable = false)
	private String gender;

	@Column(name = "number_of_children", nullable = false)
	private Integer numOfChildren;

    @Column(name = "salary", nullable = false)
	private Integer salary;

    // @Column(nullable = false, updatable = false)
    // @Temporal(TemporalType.TIMESTAMP)
    // @CreatedDate
    // private Date createdAt;

    // @Column(nullable = false)
    // @Temporal(TemporalType.TIMESTAMP)
    // @LastModifiedDate
    // private Date updatedAt;

    public Employee() {
    }

    public Employee(long id, String name, Integer nik, String address, Date birthDate, int age, Boolean maritalStatus,
            Date initialContract, Date finalContract, String gender, Integer numOfChildren, Integer salary) {
        this.id = id;
        this.name = name;
        this.nik = nik;
        this.address = address;
        this.birthDate = birthDate;
        this.age = age;
        this.maritalStatus = maritalStatus;
        this.initialContract = initialContract;
        this.finalContract = finalContract;
        this.gender = gender;
        this.numOfChildren = numOfChildren;
        this.salary = salary;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNik() {
        return nik;
    }

    public void setNik(Integer nik) {
        this.nik = nik;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Boolean getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Boolean maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Date getInitialContract() {
        return initialContract;
    }

    public void setInitialContract(Date initialContract) {
        this.initialContract = initialContract;
    }

    public Date getFinalContract() {
        return finalContract;
    }

    public void setFinalContract(Date finalContract) {
        this.finalContract = finalContract;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getNumOfChildren() {
        return numOfChildren;
    }

    public void setNumOfChildren(Integer numOfChildren) {
        this.numOfChildren = numOfChildren;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    
}
