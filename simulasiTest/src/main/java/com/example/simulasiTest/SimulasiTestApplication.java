package com.example.simulasiTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulasiTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimulasiTestApplication.class, args);
	}

}
