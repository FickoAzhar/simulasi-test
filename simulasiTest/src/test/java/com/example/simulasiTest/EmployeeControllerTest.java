package com.example.simulasiTest;

import com.example.simulasiTest.Repository.EmployeeRepository;
import com.example.simulasiTest.model.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeRepository employeeRepo;

    //===============================================================================
                                    //TEST CREATE EMPLOYEE SUCCESS
    //===============================================================================
    @Test
    public void testCreateEmployee_Success() throws Exception {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("John Doe");
        employee.setNik(2221);
        employee.setAddress("jalangerilya");
        employee.setAge(20);
        employee.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-11"));
        employee.setMaritalStatus(false);
        employee.setInitialContract(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-11"));
        employee.setFinalContract(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-11"));
        employee.setGender("L");
        employee.setNumOfChildren(1);
        employee.setSalary(1000000);

        when(employeeRepo.save(any(Employee.class))).thenReturn(employee);

        String requestBody = "{\"name\": \"John Doe\", \"nik\": 2221, \"address\": \"jalangerilya\", \"age\": 20, \"birthDate\": \"2016-01-11\", \"maritalStatus\": false, \"initialContract\": \"2016-01-11\", \"finalContract\": \"2016-01-11\", \"gender\": \"L\", \"numOfChildren\": 1, \"salary\": 1000000}";

        mockMvc.perform(post("/api/v1/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.status").value("201"))
                .andExpect(jsonPath("$.message").value("employee created successfully!"))
                .andExpect(jsonPath("$.data.id").value(1L))
                .andExpect(jsonPath("$.data.name").value("John Doe"))
                .andExpect(jsonPath("$.data.nik").value(2221))
                .andExpect(jsonPath("$.data.address").value("jalangerilya"))
                .andExpect(jsonPath("$.data.age").value(20))
                .andExpect(jsonPath("$.data.birthDate").exists())
                .andExpect(jsonPath("$.data.maritalStatus").value(false))
                .andExpect(jsonPath("$.data.initialContract").exists())
                .andExpect(jsonPath("$.data.finalContract").exists())
                .andExpect(jsonPath("$.data.gender").value("L"))
                .andExpect(jsonPath("$.data.numOfChildren").value(1))
                .andExpect(jsonPath("$.data.salary").value(1000000));
    }

    //===============================================================================
                                    //TEST CREATE EMPLOYEE FAILURE
    //===============================================================================
    @Test
    public void testCreateEmployee_Failure() throws Exception {
        when(employeeRepo.save(any(Employee.class))).thenThrow(new RuntimeException("Failed to create employee"));

        String requestBody = "{\"name\": \"John Doe\"}";

        mockMvc.perform(post("/api/v1/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isInternalServerError());
    }



    //===============================================================================
                                    //TEST GET ALL DATA EMPLOYEE
    //===============================================================================
    @Test
    public void testGetAllEmployee() throws Exception {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("John Doe");
        employee.setNik(2221);
        employee.setAddress("jalangerilya");
        employee.setAge(20);
        employee.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-11"));
        employee.setMaritalStatus(false);
        employee.setInitialContract(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-11"));
        employee.setFinalContract(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-11"));
        employee.setGender("L");
        employee.setNumOfChildren(1);
        employee.setSalary(1000000);

        when(employeeRepo.findAll()).thenReturn(Arrays.asList(employee));

        mockMvc.perform(get("/api/v1/employee")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("200"))
                .andExpect(jsonPath("$.message").value("Read all data employe success."))
                .andExpect(jsonPath("$.data").isArray())
                .andExpect(jsonPath("$.data[0].id").value(1L))
                .andExpect(jsonPath("$.data[0].name").value("John Doe"))
                .andExpect(jsonPath("$.data[0].nik").value(2221))
                .andExpect(jsonPath("$.data[0].address").value("jalangerilya"))
                .andExpect(jsonPath("$.data[0].age").value(20))
                .andExpect(jsonPath("$.data[0].birthDate").exists())
                .andExpect(jsonPath("$.data[0].maritalStatus").value(false))
                .andExpect(jsonPath("$.data[0].initialContract").exists())
                .andExpect(jsonPath("$.data[0].finalContract").exists())
                .andExpect(jsonPath("$.data[0].gender").value("L"))
                .andExpect(jsonPath("$.data[0].numOfChildren").value(1))
                .andExpect(jsonPath("$.data[0].salary").value(1000000))
                .andExpect(jsonPath("$.total").value(1));
    }


    //===============================================================================
                    //TEST GET EMPLOYEE BY NIK SUCCESS
    //===============================================================================
    @Test
    public void testGetEmployeeById_WithDataFound() throws Exception {
        int nik = 12345;
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("John Doe");
        employee.setNik(nik);
        // Set other properties for employee
        employee.setAddress("123 Main St");
        employee.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"));
        employee.setAge(32);
        employee.setMaritalStatus(true);
        employee.setInitialContract(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
        employee.setFinalContract(new SimpleDateFormat("yyyy-MM-dd").parse("2022-01-01"));
        employee.setGender("M");
        employee.setNumOfChildren(2);
        employee.setSalary(1200000);

        when(employeeRepo.findEmployeeByNik(nik)).thenReturn(Optional.of(employee));

        mockMvc.perform(get("http://localhost:8080/api/v1/employee/nik/{nik}", nik)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("200"))
                .andExpect(jsonPath("$.message").value("Read Data Employee Success!"))
                .andExpect(jsonPath("$.data.id").value(1L))
                .andExpect(jsonPath("$.data.name").value("John Doe"))
                .andExpect(jsonPath("$.data.nik").value(nik))
                // Verify other properties as needed
                .andExpect(jsonPath("$.data.address").value("123 Main St"))
                .andExpect(jsonPath("$.data.age").value(32))
                .andExpect(jsonPath("$.data.gender").value("M"))
                .andExpect(jsonPath("$.data.salary").value(1200000));
    }


    //===============================================================================
                        //TEST GET EMPLOYEE BY NIK NOT FOUND
    //===============================================================================
    @Test
    public void testGetEmployeeById_DataNotFound() throws Exception {
        int nonExistentNik = 99999; // NIK yang tidak ada dalam database

        when(employeeRepo.findEmployeeByNik(nonExistentNik)).thenReturn(Optional.empty());

        mockMvc.perform(get("http://localhost:8080/api/v1/employee/nik/{nik}", nonExistentNik)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("404"))
                .andExpect(jsonPath("$.message").value("Read Data Employee Failed. Id Not Found!"));
    }


    //===============================================================================
                    //TEST GET EMPLOYEE BY NIK SUCCESS
    //===============================================================================
    @Test
    public void testGetEmployeeByName_WithDataFound() throws Exception {
        String name = "John Doe";
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName(name);
        employee.setNik(12345);
        // Set other properties for employee
        employee.setAddress("123 Main St");
        employee.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"));
        employee.setAge(32);
        employee.setMaritalStatus(true);
        employee.setInitialContract(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
        employee.setFinalContract(new SimpleDateFormat("yyyy-MM-dd").parse("2022-01-01"));
        employee.setGender("M");
        employee.setNumOfChildren(2);
        employee.setSalary(1200000);

        when(employeeRepo.findEmployeeByName(name)).thenReturn(Optional.of(employee));

        mockMvc.perform(get("http://localhost:8080/api/v1/employee/name/{name}", name)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("200"))
                .andExpect(jsonPath("$.message").value("Read Data Employee Success!"))
                .andExpect(jsonPath("$.data.id").value(1L))
                .andExpect(jsonPath("$.data.name").value(name))
                .andExpect(jsonPath("$.data.nik").value(12345))
                // Verify other properties as needed
                .andExpect(jsonPath("$.data.address").value("123 Main St"))
                .andExpect(jsonPath("$.data.age").value(32))
                .andExpect(jsonPath("$.data.gender").value("M"))
                .andExpect(jsonPath("$.data.salary").value(1200000));
    }

    //===============================================================================
                        //TEST GET EMPLOYEE BY NIK NOT FOUND
    //===============================================================================
    @Test
    public void testGetEmployeeByName_DataNotFound() throws Exception {
        String nonExistentName = "Non Existent Name"; // Nama yang tidak ada dalam database

        when(employeeRepo.findEmployeeByName(nonExistentName)).thenReturn(Optional.empty());

        mockMvc.perform(get("http://localhost:8080/api/v1/employee/name/{name}", nonExistentName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("404"))
                .andExpect(jsonPath("$.message").value("Read Data Employee Failed. Id Not Found!"));
    }



    //===============================================================================
                                    //TEST DELETE EMPLOYEE SUCCESS
    //===============================================================================
    @Test
    public void testDeleteEmployee_Success() throws Exception {
        Employee existingEmployee = new Employee();
        existingEmployee.setId(1L);
        existingEmployee.setName("John Doe");
        existingEmployee.setNik(12345);
        existingEmployee.setAddress("123 Main St");
        existingEmployee.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"));
        existingEmployee.setAge(32);
        existingEmployee.setMaritalStatus(true);
        existingEmployee.setInitialContract(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
        existingEmployee.setFinalContract(new SimpleDateFormat("yyyy-MM-dd").parse("2022-01-01"));
        existingEmployee.setGender("M");
        existingEmployee.setNumOfChildren(2);
        existingEmployee.setSalary(1200000);

        when(employeeRepo.findById(1L)).thenReturn(Optional.of(existingEmployee));
        doNothing().when(employeeRepo).deleteById(1L);

        mockMvc.perform(delete("http://localhost:8080/api/v1/delete/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("200"))
                .andExpect(jsonPath("$.message").value("Data deleted successfully."));
    }

    //===============================================================================
                                    //TEST DELETE EMPLOYEE NOT FOUND
    //===============================================================================
    @Test
    public void testDeleteEmployee_DataNotFound() throws Exception {
        when(employeeRepo.findById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(delete("http://localhost:8080/api/v1/delete/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("404"))
                .andExpect(jsonPath("$.message").value("Delete data fail. Data not found."));
    }


    //===============================================================================
                                    //TEST UPDATE EMPLOYEE SUCCESS
    //===============================================================================
    @Test
    public void testUpdateEmployee_Success() throws Exception {
        Employee existingEmployee = new Employee();
        existingEmployee.setId(1L);
        existingEmployee.setName("John Doe");
        existingEmployee.setNik(12345);
        existingEmployee.setAddress("123 Main St");
        existingEmployee.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"));
        existingEmployee.setAge(32);
        existingEmployee.setMaritalStatus(true);
        existingEmployee.setInitialContract(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
        existingEmployee.setFinalContract(new SimpleDateFormat("yyyy-MM-dd").parse("2022-01-01"));
        existingEmployee.setGender("M");
        existingEmployee.setNumOfChildren(2);
        existingEmployee.setSalary(1200000);

        Employee updatedEmployee = new Employee();
        updatedEmployee.setId(1L);
        updatedEmployee.setName("Updated Name");
        updatedEmployee.setNik(12345);
        updatedEmployee.setAddress("123 Updated St");
        updatedEmployee.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1992-02-02"));
        updatedEmployee.setAge(30);
        updatedEmployee.setMaritalStatus(false);
        updatedEmployee.setInitialContract(new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-01"));
        updatedEmployee.setFinalContract(new SimpleDateFormat("yyyy-MM-dd").parse("2023-01-01"));
        updatedEmployee.setGender("F");
        updatedEmployee.setNumOfChildren(1);
        updatedEmployee.setSalary(1500000);

        when(employeeRepo.findById(1L)).thenReturn(Optional.of(existingEmployee));
        when(employeeRepo.save(existingEmployee)).thenReturn(updatedEmployee);

        String requestBody = "{\"name\": \"Updated Name\", \"nik\": 12345, \"address\": \"123 Updated St\", \"birthDate\": \"1992-02-02\", \"age\": 30, \"maritalStatus\": false, \"initialContract\": \"2021-01-01\", \"finalContract\": \"2023-01-01\", \"gender\": \"F\", \"numOfChildren\": 1, \"salary\": 1500000}";

        mockMvc.perform(put("http://localhost:8080/api/v1/update/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("200"))
                .andExpect(jsonPath("$.message").value("Data updated successfully."))
                .andExpect(jsonPath("$.data.id").value(1L))
                .andExpect(jsonPath("$.data.name").value("Updated Name"))
                .andExpect(jsonPath("$.data.salary").value(1500000));
    }


    //===============================================================================
                                    //TEST UPDATE EMPLOYEE NOT FOUND
    //===============================================================================
    @Test
    public void testUpdateEmployee_DataNotFound() throws Exception {
        long nonExistentId = 999L; // ID yang tidak ada dalam database

        when(employeeRepo.findById(nonExistentId)).thenReturn(Optional.empty());

        String requestBody = "{\"name\": \"Updated Name\", \"nik\": 12345, \"address\": \"123 Updated St\", \"birthDate\": \"1992-02-02\", \"age\": 30, \"maritalStatus\": false, \"initialContract\": \"2021-01-01\", \"finalContract\": \"2023-01-01\", \"gender\": \"F\", \"numOfChildren\": 1, \"salary\": 1500000}";

        mockMvc.perform(put("http://localhost:8080/api/v1/update/{id}", nonExistentId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value("404"))
                .andExpect(jsonPath("$.message").value("Update data fail. Data not found."));
    }
}











